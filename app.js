#!/usr/bin/env node

'use strict';

var fs = require('fs');
var html = require('./src/html.js');
var css = require('./src/css.js');
var font = require('./src/font.js');
var path = require('./src/path.js');


function help_out(){
    console.log('-h(--help)\t\t\t-help');
    console.log('\nexample:\nnode font-minificator -o ./path/to/new/font -i ./path/to/project/ -s 1024');
    console.log('\n-i\t\t\tpath to project');
    console.log('\n-o\t\t\tpath to new font file (without extension)');
    console.log('\n-s\t\t\tvalue for "units-per-em" element in svg file');
    process.abort();
}

function error_out(message){
    console.error(message);
    process.abort();
}

if (module.parent) {
    module.exports.modify_font = function (new_path, path_to_project, scale) {
        font.build(new_path, css.fromHtml(html.fromPath(path_to_project)), scale);
    };
}
else {
    var params = {'-i':'./',
                  '-o': './new_fonts/all_fonts',
                  '-s': 1024};

    process.argv.forEach(function(val, index){
        if(val == '-h')help_out();

        if(val in params){
            if(!process.argv[index + 1]){
                error_out('The ${val} parameter has no value!');
            }
            params[val] = process.argv[index + 1];
        }
    });

    var html_files =html.fromPath(params['-i']) ;

    if(!html_files.length){
        console.log('No html files found!');
        process.abort();
    }
    font.build(params['-o'],css.fromHtml(html_files), params['-s']);

}
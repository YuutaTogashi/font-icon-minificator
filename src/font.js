/**
 * Created by kamina on 14.06.16.
 */
var fs = require('fs');
var path = require('./path.js');
var d_commands = require('./d_commands');
var svg2ttf = require('svg2ttf');
var ttf2woff = require('ttf2woff');
var ttf2eot = require('ttf2eot');


var scale_coefficient = 0.9375;

function reg_exp_font_icon(code) {
    return new RegExp('<glyph unicode="&#x' + code + '((.|\\n)*?)\\/>', 'm');
}

var reg_exp_font_path = /url\(([-\w\/\.\s']*?)\.svg/gm;

function wrap_font(icons, units_per_em) {
    var descent = (units_per_em - (units_per_em * scale_coefficient));
    var ascent = units_per_em * scale_coefficient;
    return '<?xml version="1.0" standalone="no"?>\n' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" >\n'
        + '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"\n>'
        + '<metadata></metadata>\n'
        + '<defs>\n'
        + '<font id="minified" horiz-adv-x="'+units_per_em+'" >\n'
        + '<font-face units-per-em="' + units_per_em + '" ascent="'+ascent+'" descent="-'+descent+'" />\n'
        + '<missing-glyph horiz-adv-x="448" />\n'
        + icons
        + '</font></defs></svg>\n';
}


function extract_path_to_font(css_file) {
    var font_path = css_file.match(/@font-face(\s*)\{((.|\n)*?)\}/gm)[0];
    font_path = font_path.match(reg_exp_font_path)[0];

    if (font_path.match(/'(.*?)'/gm)) {
        font_path = font_path.match(/'(.*?)'/gm)[0];
        return font_path.slice(1, font_path.length - 1);
    }
    return font_path.match(/([\w\/\.-]*?)\.svg/gm)[0];
}

function is_code_repead(collection, code) {
    var counter = 0;
    for (var row = 0; row < collection.length; row++) {

        for (var elem = 0; elem < collection[row]['sets'].length; elem++) {
            if (code == collection[row]['sets'][elem]['code']) {
                counter++;
            }
            if (counter >= 2) return true;
        }
    }
    return false;
}


function is_code_used(collection, code) {
    var counter = 0;
    for (var row = 0; row < collection.length; row++) {

        for (var elem = 0; elem < collection[row]['sets'].length; elem++) {
            if (code == collection[row]['sets'][elem]['code']) {
                counter++;
            }
            if (counter >= 1) return true;
        }
    }
    return false;
}


function number_to_code(number) {
    var str = number.toString(16);
    var res = str;

    for (var i = 0; i < (4 - str.length); i++) {
        res = '0' + res;
    }
    return res;
}


function get_icon(code, font_file, scale, code_to_change) {
    var result = font_file.match(reg_exp_font_icon(code));
    if (!result) return null;

    result = result[0];
    if (code_to_change) {
        result = result.replace(/unicode(\s*)=(\s*)"&#x([A-Za-z\d]{4});"/gm, 'unicode="&#x'+code_to_change+';"');

    }
    var units_per_em = scale;
    if(font_file.match(/units-per-em="(\d+?)"/))
        units_per_em = font_file.match(/units-per-em="(\d+?)"/)[1];

    var ascent = scale*scale_coefficient;
    if(font_file.match(/ascent="(\d+?)"/))
        ascent = font_file.match(/ascent="(\d+?)"/)[1];

    var font_scale_coefficent = ascent/units_per_em;

    if((scale/units_per_em) != 1){
        var d_attr = result.match(/d="((.|\n)+?)"/gm)[0];

        d_attr = d_attr.replace(/(\d+(\.?))+/gm, function (str){
            if(str.indexOf('M') !=-1 || str.indexOf('m') != -1) return str;
            var number = +str;

            return (number*(scale/units_per_em)) + "";
        });
        if(scale_coefficient != font_scale_coefficent) {
            var difference = scale * Math.abs(scale_coefficient-font_scale_coefficent);
            d_attr = 'd="' + d_commands.modify_d_element(d_attr, difference * -1) +'"';
        }
        result = result.replace(/d="((.|\n)+?)"/gm, d_attr);
    }

    return result;
}


function build(path_to_out, collection, scale) {
    var code_to_change = 40960;
    var result_file = '';
    var size = 0;
    
    for (var row = 0; row < collection.length; row++) {

        var css_file = fs.readFileSync(collection[row]['path'], 'utf8');
        var changed_classes = [];

        var font_path = path.get_dir(collection[row]['path']) + extract_path_to_font(css_file);
        size += fs.statSync(font_path).size;
        var font_file = fs.readFileSync(font_path, 'utf8');
        var font_scale = scale;


        if(font_file.match(/units-per-em="(\d+?)"/))
            font_scale = font_file.match(/units-per-em="(\d+?)"/)[1];

        font_file = font_file.replace(/horiz-adv-x="(\d+?)"/gm, 'horiz-adv-x="' + scale + '"');

        for (var elem = 0; elem < collection[row]['sets'].length; elem++) {
            var code = collection[row]['sets'][elem]['code'];

            if (is_code_repead(collection, code)) {
                while (is_code_used(collection, number_to_code(code_to_change))) code_to_change++;
                var temp = get_icon(code, font_file, (scale), number_to_code(code_to_change));

                if (temp != null) result_file += temp + '\n';
                changed_classes.push({
                    "old": collection[row]['sets'][elem]['code'],
                    "new": number_to_code(code_to_change)
                });

                collection[row]['sets'][elem]['code'] = number_to_code(code_to_change);
            }

            else {
                //result_file += get_icon(code, font_file);
                var temp = get_icon(code, font_file, (scale));
                if (temp != null) result_file += temp + '\n';
            }
        }

        path.modify_css(path_to_out, collection[row]['path'], changed_classes);
    }
    if(!result_file){
        console.log('Sorry, but no icons found...');
        process.abort();
    }
    path.mkpath(path_to_out + '.svg');
    fs.writeFileSync(path_to_out + '.svg', wrap_font(result_file, scale));

    var ttf = svg2ttf(fs.readFileSync(path_to_out + '.svg', 'utf-8'), {});
    fs.writeFileSync(path_to_out + '.ttf', new Buffer(ttf.buffer));

    var eot = ttf2eot(fs.readFileSync(path_to_out + '.ttf'), {});
    fs.writeFileSync(path_to_out + '.eot', new Buffer(ttf.buffer));

    var woff = ttf2woff(fs.readFileSync(path_to_out + '.ttf'), {});
    fs.writeFileSync(path_to_out + '.woff', new Buffer(ttf.buffer));

}
module.exports.build = build;






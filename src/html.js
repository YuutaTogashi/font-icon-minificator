/**
 * Created by kamina on 09.06.16.
 */
var fs = require('fs');
var path = require('./path.js');


function get_html_files(project_path){
    var html_files = [];
    var temp_files = fs.readdirSync(project_path,'utf8');
    temp_files.forEach(function (file){
        if (file.search(/\.html?\b/) != -1 && !fs.statSync(project_path + file).isDirectory()){
            html_files.push(project_path + file);
        }
        if (!file.match(/\./) && fs.statSync(project_path  + file ).isDirectory()){
            var other_files = get_html_files(project_path + file + '/');

            if (other_files.length > 0) {
                html_files = html_files.concat(other_files);
            }
        }
    });

    return html_files;
}
module.exports.fromPath = get_html_files;

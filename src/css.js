/**
 * Created by kamina on 09.06.16.
 */
var sax = require('sax');
var path = require('./path.js');
var fs = require('fs');

//var css_class_reg = /\.([^\s\d])([\w-_]*?)(?=(((\s*):(\s*)([a-zA-Z-_]*))?(\s*\{)))/gm;

var css_class_reg = /(\.([^\s\d])(([-\w_\.])*?)((\s*):(\s*)([a-zA-Z-_]*))*(([\s\n]*)\,([\s\n]*))*)+(?=(\s*\{))/gm;
function is_font_icon(path_to_css){
    if(fs.existsSync(path_to_css)){
        var css_file = fs.readFileSync(path_to_css, 'utf8');
        if(css_file.search(/@font-face/m) != -1){
            return true;
        }
        return false;
    }
}

function get_content (css_file, class_name){
    var reg_exp = new RegExp(class_name.replace('.', '\\.') + '((.|\\n)*?)\\}', 'gm')
    var class_body = css_file.match(reg_exp);
    if(class_body != null){
        var content = class_body[0].match(/content(:*)(.*);/gm);
        if(content == null) return null;
        var code = content[0].match(/\\\w{4}/gm);
        if(code == null) return null;
        return code[0].slice(1);
    }
    return null;
}


function is_already_was(value, array, current_position)
{
    var counter = 0;
    for(var i = 0; i<current_position && i < array.length; i++)
    {
        if(array[i] == value) counter++;
        if(counter > 2) return true;
    }
    return false;
}


function css_from_html(html_path){
    var html_file = fs.readFileSync(html_path, 'utf8');
    var hrefs = [];
    var parser = sax.parser(false);
    parser.onopentag = function(tag){
        if(tag.name.toLowerCase() == 'link'){
        }
        if(tag.attributes.REL == 'stylesheet'){
            path_to_css = path.get_dir(html_path) + tag.attributes.HREF;
            if(is_font_icon(path_to_css)){
                hrefs.push(path_to_css);
            }
        }
    };
    parser.write(html_file).close();

    return hrefs;
}

function join_css (){
    result = [];
    for (var i = 0; i < arguments.length; i++){
        for(var j =0; j<arguments[i].length;j++){
            for (var z=0;z<result.length;z++){
                if(arguments[i][j]['name'] == result[z]['name'])continue;
            }
            result.push(arguments[i][j]);
        }
    }
    return result;
}

function filter_css (collection){
    for(var i =0;i<collection.length ;i++){
        for(var j=i+1;j<collection.length;j++){
            if(collection[i]['path'] == collection[j]['path']){
                collection[i]['sets'] = join_css(collection[i]['sets'], collection[j]['sets']);
                collection.splice(j,1);
                j--;
            }
        }
    }
    return collection;
}

function in_array(value, array){
    for(var i = 0; i<array.length;i++){
        if(value == array[i])return true;
    }
    return false;
}
function extract_classes(html_path, css_files){
    var html_file = fs.readFileSync(html_path, 'utf8');
    var result = [];
    var classes_in_html = [];
    var parser = sax.parser(false);

    parser.onattribute = function (attr){
        if(attr.name == 'CLASS' && !in_array(attr.value, classes_in_html)){
            var temp_classes = attr.value.match(/\b[A-Za-z]([\w-]+?)(?=(\s|$))/gm);
            if(temp_classes) {
                for (var i = 0; i < temp_classes.length; i++) {
                    if(!in_array('.' + temp_classes[i], classes_in_html)) {
                        classes_in_html.push('.' + temp_classes[i]);
                    }
                }
            }
        }
    };

    parser.onend = function(){
        for (var i =0;i<css_files.length; i++){
            if(!fs.existsSync(css_files[i])) continue;

            var css_file = fs.readFileSync(css_files[i], 'utf8');
            var classes = css_file.match(css_class_reg);

            if(classes && classes.length > 1) {
                classes = classes.filter(function(element, counter){
                    return in_array(element.match(/\.([^\s\d])([-\w_]*?)($|(?=:))/gm)[0], classes_in_html);
                });

                classes = classes.filter(function(element, counter){
                    return !is_already_was(element, classes, counter);
                });

                classes = classes.map(function(class_name){
                    var result = {};
                    result['name'] = class_name;
                    result['code'] = get_content(css_file, class_name);
                    return result;
                });


                classes = classes.filter(function (class_object){
                    return class_object['code'];
                });

                if(classes.length != 0) {
                    var tmp = {};
                    tmp['path'] = css_files[i];
                    tmp['sets'] = classes;
                    result.push(tmp);
                }
            }
        }
    };
    parser.write(html_file).close();

    return result;
}


function css_getter(html_array){
    var result = [];
    for(var i = 0; i< html_array.length; i++){
        var css_files = css_from_html(html_array[i]);
        if(css_files.length>0){
            temp = extract_classes(html_array[i], css_files);
            result = result.concat(temp);
        }
    }
    result = result.filter(function(obj){
        return obj;
    });

    result = filter_css(result);
    return result;
}

module.exports.fromHtml = css_getter;
module.exports.fromPath = function (path){
    return css_getter(html.fromPath(path));
};
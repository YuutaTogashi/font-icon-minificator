/**
 * Created by kamina on 12.07.16.
 */
function change_by_step(step){
    return function(data, difference){
        var result = data.match(/(-?)(\d+(\.?))+/gm);
        for (var i=step-1;i<result.length;i+=step){
            result[i] -= difference;
        }

        return data[0]+result.join(',');
    };
}

function change_by_pos(pos){
    return function(data, difference){
        var result = data.match(/(\d+(\.?))+/gm);

        return result [pos-1] - difference;
    }
}

function modify_d_element (data, difference){
    var svg_command_reg_exp = /z|([A-Za-z])((\s*)(((-?)\d+(\.?))+)(\s*)(,?)(\s*))+(?=[a-zA-Z])/gm;
    var modify_by_command = {
        'M': change_by_step(2),
        'V': change_by_pos(1),
        'L': change_by_step(2),
        'C': change_by_step(2),
        'S': change_by_step(2),
        'Q': change_by_step(2),
        'T': change_by_step(2),
        'A': change_by_step(6),
    };
    var commands = data.match(svg_command_reg_exp);
    var result = '';
    for(var i = 0; i<commands.length;i++){
        if(!(commands[i][0] in modify_by_command)) {
            result+=commands[i];
            continue;
        }
        result+=modify_by_command[commands[i][0]](commands[i], difference);
    }
    return result;
}
module.exports.modify_d_element = modify_d_element;
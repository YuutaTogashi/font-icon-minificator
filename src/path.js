/**
 * Created by kamina on 17.06.16.
 */
var fs = require('fs');


function get_dir(path_to_file)
{
    var result = path_to_file.match(/^.+\//m);
    if (result)
        return result[0];
}


function mkpath(path){
    var path_array = path.split('/');
    var next_step = '';
    for(var i =0 ;i<path_array.length;i++){
        next_step += path_array[i] +'/';
        if (!fs.existsSync(next_step)){
            if(i == path_array.length -1 && path[path.length -1] != '/'){
                next_step = next_step.slice(0, next_step.length -1);
                fs.writeFileSync(next_step, '');
                break;
            }
            fs.mkdirSync(next_step);
        }
    }
}



function related_path_to(to, from){
    //to = process.cwd()+ '/' + to;
    //from  = __dirname + from;

    var to_arr = to.split('/');
    var from_arr = from.split('/');
    var result = '';
    var diff_ways = 0;

    while(to_arr[diff_ways] == from_arr[diff_ways])diff_ways++;

    for(var i=0; i<(from_arr.length - diff_ways - 1); i++){
        result += '../';
    }
    for(var i=diff_ways; i<to_arr.length; i++){
        result += to_arr[i];
        if(i != to_arr.length -1 ) result += '/';
    }
    return result;
}


function modify_css (path_to_font, path_to_css, changed_classes){
    var regexp_url = /url\(('?)([\w\/\.-]*)(?=(\.))/gm;
    var input = fs.readFileSync(path_to_css, 'utf-8');

    var path_to_input = 'url(\'' + related_path_to(path_to_font, get_dir(path_to_css));

    input = input.replace(regexp_url, path_to_input);

    if(changed_classes && 'length' in changed_classes){
        for (var i =0;i<changed_classes.length;i++){
            input = input.replace(RegExp('content(\\s*):(\\s*)"\\\\'+changed_classes[i]['old'] +'"(\\s*);', 'gm'),
                                    'content: "\\' +changed_classes[i]['new'] + '";');
        }
    }

    fs.writeFileSync(path_to_css + '.tmp', input);

    fs.writeFileSync(path_to_css, fs.readFileSync(path_to_css + '.tmp'));
    fs.unlink(path_to_css + '.tmp');

    //var input  = fs.createReadStream(path_to_css, {encoding: 'utf-8'});
    //fs.create
    //var output = fs.createWriteStream(path_to_css + '.tmp', 'utf8');
    //var path_to_input = 'url(\'' + related_path_to(path_to_font, get_dir(path_to_css));
    //
    //input.on('data', (chunk) => {
    //    if(chunk.search(regexp_url) != -1){
    //            output.write(chunk.replace(regexp_url, path_to_input));
    //        }
    //    output.write(chunk);
    //    console.log('write');
    //});
    //
    //input.on('close',function(){
    //    console.log('close');
    //    output.end();
    //});
    //
    //output.on('finish', () =>{
    //    console.log('copy')
    //    fs.writeFileSync(path_to_css, fs.readFileSync(path_to_css + '.tmp'));
    //    console.log('deleting');
    //    fs.unlink(path_to_css + '.tmp');
    //} );
}

module.exports.modify_css = modify_css;
module.exports.related_path_to = related_path_to;
module.exports.get_dir = get_dir;
module.exports.mkpath = mkpath;